import React from 'react';

// data
import coursesData from '../data/courseData';

// react-bootstrap components
import { Col, Container, Row } from 'react-bootstrap';

// Components
import CourseCard from '../components/CoursesCard/CourseCard';

const Courses = () => {
    return (
        <Container className='my-5'>
            <h1 className="text-center">Courses</h1>
            <Row>
                {
                    coursesData.map(data => {
                        return (
                            <Col key={data.id}>
                                <CourseCard 
                                    title = {data.name}
                                    desc = {data.description}
                                    price = {data.price}
                                />
                            </Col>
                        )
                    })
                }
            </Row>
        </Container>
    )
}

export default Courses;