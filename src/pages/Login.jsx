import React, { useState, useEffect, useContext } from 'react';

// user context
import UserContext from '../UserContext';

// react-bootstrap components
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Row 
} from 'react-bootstrap';

// react-router
import { useHistory } from 'react-router-dom';


const Login = () => {
    // useContext is used to unpack the data from the UserContext
    const { user, setUser } = useContext(UserContext);
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });
    const [isDisabled, setIsDisabled] = useState(true);
    const history = useHistory();
    
    // Form Data destructuring
    const { email, password } = formData;

    // Use Effect
    useEffect(() => {
        if( email !== "" && password !== "" ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password])

    // events handler
    const handleChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});
    
    const handleSubmit = (e) => {
        e.preventDefault();
        alert("You are now logged in!");
        
        localStorage.setItem("email", formData.email);
        setUser({
            email: localStorage.getItem("email")
        });
        history.push('/');
        setFormData({
            email: "",
            password: ""
        })
    }
    return (
        <Container className="my-5">
            <h1 className="text-center">Login</h1>
            <Row className="justify-content-center mt-4">
                <Col xs={10} md={6}>
                    <Form
                        className="border p-3"
                        onSubmit={handleSubmit}
                    >

                        <Form.Group 
                            className="mb-3" 
                            controlId="email"
                        >
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.email}
                                name="email"
                                type="email" 
                                placeholder="Enter email" 
                            />
                        </Form.Group>

                        <Form.Group 
                            className="mb-3" 
                            controlId="password"
                        >
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.password}
                                name="password"
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Group>

                        <Button 
                            variant="primary" 
                            type="submit"
                            className="btn-info"
                            disabled={isDisabled}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Login
